import pickle
import time
import sys
import numpy as np
import tensorflow as tf
from docopt import docopt
from sklearn.feature_extraction.text import CountVectorizer
import os
from sklearn.utils import shuffle
from concurrent import futures

np.random.seed(3306)


# transforms sentence into a list of indices.
def get_idx_from_sent(sent, word_idx_map, maxlen, padding):
    x = []
    label = int(sent[-1])
    sent = sent[:-1]
    for i in range(padding):
        x.append(0)
    words = sent.split()
    for index, word in enumerate(words):
        if word in word_idx_map:
            x.append(word_idx_map[word])
    while len(x) < maxlen + 2 * padding:
        x.append(0)
    x.append(label)
    x.append(len(words))
    return x


def process_data(sents, word_idx_map, maxlen, padding):
    data = [get_idx_from_sent(sent, word_idx_map, maxlen, padding) for sent in sents]
    return data


# process datasets as 10-fold validation.
def make_idx_data_cv(sentences, word_idx_map, cv, maxlen, padding, config, log_path):
    if not os.path.exists(log_path):
        os.makedirs(log_path)

    train_sents = []
    test_sents = []
    dev_sents = None
    label_set = set()

    if config['dataset'] in ['mr', 'subj', 'cr', 'mpqa']:
        for sen in sentences:
            # datum = {'y': label,
            #          'text': orig_rev,
            #          'num_words': len(orig_rev.split()),
            #          'split': np.random.randint(0, cv)}
            # s = get_idx_from_sent(sen['text'], word_idx_map, maxlen, padding)
            # s.append(sen['y'])
            sen_w = sen['text'] + str(sen['y'])
            label_set.add(sen['y'])
            if sen['split'] == cv:
                test_sents.append(sen_w)
            else:
                train_sents.append(sen_w)
    elif config['dataset'] in ['sst1', 'sst2']:
        dev_sents = []
        for sen in sentences:
            sen_w = sen['text'] + str(sen['y'])
            label_set.add(sen['y'])
            if sen['split'] == 'test':
                test_sents.append(sen_w)
            elif sen['split'] == 'train':
                train_sents.append(sen_w)
            elif sen['split'] == 'dev':
                dev_sents.append(sen_w)
    elif config['dataset'] in ['trec', 'ag_news']:
        for sen in sentences:
            sen_w = sen['text'] + str(sen['y'])
            label_set.add(sen['y'])
            if sen['split'] == 'test':
                test_sents.append(sen_w)
            elif sen['split'] == 'train':
                train_sents.append(sen_w)

    label_class = len(label_set)
    config['label_class'] = label_class
    # print(config)

    train_sents, test_sents, dev_sents, W_score, count_ref, w_score_come_from = my_process(train_sents, test_sents,
                                                                                    word_idx_map, config, log_path, label_class, dev_sents)
    # [train_pos_sents, train_neg_sents], [test_pos_sents, test_pos_sents], [train_pos_mask, train_neg_mask]
    train = process_data(train_sents, word_idx_map, maxlen, padding)
    test = process_data(test_sents, word_idx_map, maxlen, padding)
    dev = process_data(dev_sents, word_idx_map, maxlen, padding)

    train_mask_ratio = return_mask_ratio(train, W_score)

    train = np.asarray(train)
    test = np.asarray(test)
    dev = np.asarray(dev)

    return [train, test, dev, train_mask_ratio], W_score, count_ref, w_score_come_from

def return_mask_ratio(train_sents_id, W_score):
    # return all mask ratio for train
    train_mask_ratio = []
    for index, sent in enumerate(train_sents_id):
        mask_ratio = []
        sent = sent[:-2]
        for word in sent: # the word of sent is id
            mask_ratio.append(W_score[word])
        train_mask_ratio.append(mask_ratio)
        # print(mask_ratio)
    return train_mask_ratio

def my_process(train_sents, test_sents, word_idx_map, config, logpath, label_class, dev_sents=None):
    np.random.shuffle(train_sents)
    if not dev_sents:
        dev_sents = train_sents[int(len(train_sents) * 0.9):]
        train_sents = train_sents[: int(len(train_sents) * 0.9)]

    # all_data = [[], []]  # [neg, pos]
    all_data = [[] for _ in range(label_class)]
    label_ = [str(i) for i in range(label_class)]
    for sent in train_sents:
        index = label_.index(sent[-1])
        all_data[index].append(sent[:-1])

    all_data = [' '.join(s) for s in all_data]

    def score_ratio(count, smoothing_alpha, vocabulary):
        count += smoothing_alpha

        ratio = []
        for index in range(count.shape[0]):
            class_count_ratio = count[index] / np.sum(count[index])
            other_count = np.sum(count, 0) - count[index]
            other_class_count_ratio = other_count / np.sum(other_count)
            num_count_scale = 1
            class_ratio = class_count_ratio * num_count_scale / other_class_count_ratio
            class_ratio = np.array(class_ratio)
            ratio.append(class_ratio)
        ratio = np.array(ratio)
        ratio_max = np.max(ratio, axis=0)
        beta = config['beta']
        ratio_max_scale = (np.exp(beta * ratio_max) - 1) / (np.exp(beta * ratio_max) + 1)
        count_word_score_come_from = np.argmax(ratio, axis=0)
        return ratio_max, ratio_max_scale, count_word_score_come_from

    my_vectorizer = CountVectorizer(ngram_range=(1, 1), min_df=1)
    count = my_vectorizer.fit_transform(all_data).toarray()
    vocabulary = my_vectorizer.get_feature_names()
    word_score_before_scale, word_score, count_word_score_come_from = score_ratio(count, config['smoothing_alpha'], vocabulary)

    W_score = [0]  # the id_word_map start with 0
    W_score_before_scale = [0]
    count_ref = ['pad']
    id_word_map = {j: i for i, j in word_idx_map.items()}
    w_score_come_from = {} # len() == len(W)
    max_prob = 0.8
    for w_i in range(1, len(id_word_map) + 1):
        word = id_word_map[w_i]
        if word in vocabulary:
            index = vocabulary.index(word)
            tmp_score = word_score[index]
            if tmp_score > max_prob:
                tmp_score = max_prob
            W_score.append(tmp_score)
            W_score_before_scale.append(word_score_before_scale[index])
            this_class = count[count_word_score_come_from[index]][index]
            other_class = np.sum(count[:, index]) - count[count_word_score_come_from[index]][index]
            count_ref.append('[{} {}]'.format(this_class, other_class))
            w_score_come_from[w_i] = count_word_score_come_from[index]
        else:
            W_score.append(0)
            W_score_before_scale.append(0)
            count_ref.append('not_in')
    id_word_map[0] = 'pad'

    with open('{}/wordscore'.format(logpath), 'w') as f:
        k = len(W_score)
        W_score = np.array(W_score)
        W_score = np.float32(W_score)

        top_k = W_score.argsort()[(-1) * k:][::-1]
        # max_prob = (W_score[top_k[0]] - 1) / config['ratio_divide']
        # config['ratio_max_cal'] = max_prob
        config['prob_view'] = '{}_{}_{}_{}'.format(W_score[top_k[0]], W_score[top_k[10]], W_score[top_k[100]], W_score[top_k[200]])
        print(config)
        for index in top_k:
            w_from = 'no'
            if index in w_score_come_from:
                w_from = w_score_come_from[index]
            f.write('{}\t{:.2f}\t{:.2f}\t{}\t{}\n'.format(id_word_map[index], W_score[index], W_score_before_scale[index], count_ref[index], w_from))
    return train_sents, test_sents, dev_sents, W_score, count_ref, w_score_come_from


# train each.
def train_cv(datasets,
             W,
             W_score,
             W_score_ref,
             count_ref,
             w_score_come_from,
             config,
             log_path,
             word_idx_map,
             maxlen,
             embedding_dims=300,
             dropoutd=0.5,
             batch_size=50,
             nb_epoch=50,
             nb_filter=100,
             filter_length=[3, 4, 5],
             hidden_dim=2,
             norm_lim=3,
             data_split=0,
             ):
    if config['debug']:
        datasets[0] = datasets[0][:202]
        datasets[1] = datasets[1][:202]
        datasets[2] = datasets[2][:202]
        datasets[3] = datasets[3][:202]
        permutation = np.arange(len(datasets[0]))
    else:
        permutation = np.random.permutation(len(datasets[0]))

    print('CV: ' + str(data_split + 1))

    X_train = np.asarray([d[:-2] for d in datasets[0]])
    # X_train_mask = np.asarray([mask for d, mask in datasets[0]])
    Y_train = np.asarray([d[-2] for d in datasets[0]])
    X_train_len  = np.asarray([d[-1] for d in datasets[0]])


    X_test = np.asarray([d[:-2] for d in datasets[1]])
    # X_test_mask = np.asarray([mask for d, mask in datasets[1]])
    Y_test = np.asarray([d[-2] for d in datasets[1]])
    X_test_len = np.asarray([d[-1] for d in datasets[1]])

    X_dev = np.asarray([d[:-2] for d in datasets[2]])
    Y_dev = np.asarray([d[-2] for d in datasets[2]])
    X_dev_len= np.asarray([d[-1] for d in datasets[2]])


    X_train_mask_ratio = np.array(datasets[3])

    X_train = X_train[permutation]
    Y_train = Y_train[permutation]
    X_train_mask_ratio = X_train_mask_ratio[permutation]

    def softmaxY(Y):
        newY = []
        for y in Y:
            tmpY = [0] * hidden_dim
            tmpY[y] = 1
            newY.append(tmpY)
        return np.asarray(newY)
    # [1, 0] neg
    # [0, 1] pos

    Y_train = softmaxY(Y_train)
    Y_test = softmaxY(Y_test)
    Y_dev = softmaxY(Y_dev)

    print('X_train shape:', X_train.shape)
    print('X_train_mask_ratio shape:', X_train_mask_ratio.shape)
    print('X_train_len shape:', X_train_len.shape)

    print('Y_train shape:', Y_train.shape)
    print('X_dev shape:', X_dev.shape)
    # print('X_dev_mask shape:', X_dev_mask.shape)
    print('Y_dev shape:', Y_dev.shape)
    print('X_test shape:', X_test.shape)
    # print('X_test_mask shape:', X_test_mask.shape)
    print('Y_test shape:', Y_test.shape)

    # initialize W in CNN.
    def conv_weight_variable(shape):
        initial = np.random.uniform(-0.01, 0.01, shape)
        conv_W = tf.Variable(initial, name='conv_W', dtype=tf.float32)
        return conv_W

    # initialize bias in CNN.
    def conv_bias_variable(shape):
        initial = tf.zeros(shape=shape)
        conv_b = tf.Variable(initial, name='conv_b', dtype=tf.float32)
        return conv_b

    # initialize W in fully connected layer.
    def fcl_weight_variable(shape, intial_type='normal'):
        if intial_type == 'uniform':
            initial = tf.random_uniform(-0.1, 0.1, shape)
        else:
            initial = tf.random_normal(shape=shape, stddev=0.01)
        fcl_W = tf.Variable(initial, name='fcl_W')
        return fcl_W

    # initialize bias in fully connected layer.
    def fcl_bias_variable(shape):
        initial = tf.zeros(shape=shape)
        fcl_b = tf.Variable(initial, name='fcl_b')
        return fcl_b

    # compute convolution.
    def conv1d(x, conv_W, conv_b):
        conv = tf.nn.conv1d(x,
                            conv_W,
                            stride=1,
                            padding='SAME',
                            name='conv')
        h = tf.nn.relu(tf.nn.bias_add(conv, conv_b), name='relu')
        return h

    # max-pooling.
    def max_pool(x):
        return tf.reduce_max(x, axis=1)

    # set all states to default.
    tf.reset_default_graph()
    tf.set_random_seed(1)
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8, allow_growth=True)
    sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

    # input.
    x = tf.placeholder(tf.int32, [None, maxlen], name='input_x')

    y_ = tf.placeholder(tf.float32, [None, hidden_dim], name='input_y')
    dropout_keep_prob = tf.placeholder(tf.float32, name='dropout_keep_prob')
    word_same_dropout = tf.placeholder(tf.float32, name='word_dropout')  # prob to keep, only use in baseline_same_drop


    # embedding.
    with tf.device('/cpu:0'), tf.name_scope('embedding'):

        embedding_table = tf.Variable(W, name='embedding_table')
        if config['baseline_same_drop']:
            # baseline2: words are dropped according to the same probability
            x_input = tf.cast(x, tf.float32)
            x_dropout = tf.nn.dropout(x_input, word_same_dropout)
            x_dropout = tf.clip_by_value(x_dropout, 0, 1)
            x_input = x_input * x_dropout
            x_input = tf.cast(x_input, tf.int32)
            embedded_words = tf.nn.embedding_lookup(embedding_table, x_input)
        else:
            embedded_words = tf.nn.embedding_lookup(embedding_table, x)

    if config['mask_ratio']:  # our model
        w_mask = tf.placeholder(tf.float32, [None, maxlen], name='w_mask')
        random_num = tf.random_uniform(tf.shape(w_mask), minval=0, maxval=1)
        mask_done = tf.greater_equal(random_num, w_mask)  # if w_mask > random_num, then should be masked, ie, set to 0
        mask_done = tf.cast(mask_done, tf.float32)

        embedded_words = embedded_words * tf.expand_dims(mask_done, -1)

    # CNN.
    pooled_outputs = []
    for i in filter_length:
        with tf.name_scope('conv_maxpool_%s' % i):
            filter_shape = [i, embedding_dims, nb_filter]
            conv_W = conv_weight_variable(filter_shape)
            conv_b = conv_bias_variable([nb_filter])
            conv = conv1d(embedded_words, conv_W, conv_b)
            pooled = max_pool(conv)
            pooled_outputs.append(pooled)

    nb_filter_total = nb_filter * len(filter_length)
    h_pool = tf.concat(pooled_outputs, 1)
    h_pool_flat = tf.reshape(h_pool, [-1, nb_filter_total])



    # dropout.
    if config['use_dropout']:
        with tf.name_scope('dropout'):
            if config['dropouttrain']:  # test if dropout rate is trainable, bad result
                # intial = np.random.uniform(0, 1, [nb_filter_total])
                intial = np.ones([nb_filter_total])
                intial *= 0
                dropout_w = tf.Variable(intial)  # to be droppped out

                dropout_w = tf.cast(dropout_w, tf.float32)
                tmp_dis = tf.random_uniform([nb_filter_total], 0, 1)
                tmp_ratio = tmp_dis / dropout_w  # <1 to be dropped out  <1 to be set to 0
                mask_bool = tf.greater_equal(tmp_ratio, 1)
                mask_w_fc = tf.cast(mask_bool, tf.float32)
                h_pool_flat = h_pool_flat * mask_w_fc
                # dropout_w_scale=0 in test
                is_training = tf.less(dropout_keep_prob, 1)
                is_training = tf.cast(is_training, tf.float32)
                dropout_w_scale = dropout_w * is_training
                h_pool_flat *= (1 / (1 - dropout_w_scale))

                # in training
                # h_pool_flat = tf.nn.dropout(h_pool_flat, dropout_keep_prob)
            else:
                if config['magic_scale']: # magic_scale is bad in this
                    h_pool_flat = tf.nn.dropout(h_pool_flat, dropout_keep_prob) * (1 - dropout_keep_prob + dropoutd)
                else:
                    h_pool_flat = tf.nn.dropout(h_pool_flat, dropout_keep_prob)


    # fully connected layer.
    with tf.name_scope('fcl'):
        fcl_W = fcl_weight_variable([nb_filter_total, hidden_dim])
        fcl_b = fcl_bias_variable([hidden_dim])
        fcl_output = tf.matmul(h_pool_flat, fcl_W) + fcl_b
        y = tf.nn.softmax(fcl_output)
        y = tf.clip_by_value(y, clip_value_min=1e-6, clip_value_max=1 - 1e-6)


    # loss.
    # cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y) + (1 - y_) * tf.log(1 - y), reduction_indices=[1]))

    optimizer = tf.train.AdadeltaOptimizer(learning_rate=1.0, rho=0.95, epsilon=1e-08)
    gvs = optimizer.compute_gradients(cross_entropy)
    capped_gvs = [((tf.clip_by_norm(grad, norm_lim)), var) for grad, var in gvs]
    train_step = optimizer.apply_gradients(capped_gvs)
    prediction = tf.arg_max(y, 1)
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # Train.
    tf.global_variables_initializer().run()
    saver = tf.train.Saver(max_to_keep=2)

    # print('start train')
    best_accuracy = 0
    best_loss = 100
    test_accuracy = 0
    timedelay = 0
    for e in range(nb_epoch):
        if (config['mask_ratio']) and (not config['baseline_same_drop']) and (not (config['dropouttrain'])):
            train_mask_done = X_train_mask_ratio
        else:
            train_mask_done = np.zeros_like(X_train)  # the mask prob indicates the prob to be masked


        if timedelay == config['timedelay']:
            break
        epoch_starttime = time.time()
        i = 0

        # print(batch_size)
        while i + 1 < len(X_train):
            if i + batch_size < len(X_train):
                batch_xs = X_train[i:i + batch_size]
                batch_xs_mask = train_mask_done[i:i + batch_size]
                batch_x_l = X_train_len[i:i+batch_size]
                batch_ys = Y_train[i:i + batch_size]
            else:
                batch_xs = X_train[i:]
                batch_xs_mask = train_mask_done[i:]
                batch_x_l = X_train_len[i:]
                batch_ys = Y_train[i:]
            i += batch_size
            if config['mask_ratio']:
                mask_feed = batch_xs_mask
                train_step.run({x: batch_xs, y_: batch_ys, dropout_keep_prob: dropoutd, w_mask: mask_feed, word_same_dropout: 1.0 - config['same_drop_prob']})
            else:
                train_step.run({x: batch_xs, y_: batch_ys, dropout_keep_prob: dropoutd})
        train_result = [cross_entropy]

        if config['mask_ratio']:
            xx = X_train[-10 * batch_size:]
            mask_feed = train_mask_done[-10 * batch_size:]
            train_loss = sess.run(train_result,
                                  feed_dict={x: xx,
                                             # x_mask: X_train_mask[-10 * batch_size:],
                                             y_: Y_train[-10 * batch_size:],
                                             w_mask: mask_feed,
                                             word_same_dropout: 1.0 - config['same_drop_prob'],
                                             dropout_keep_prob: 1.0})


        else:
            train_loss = sess.run(train_result,
                              feed_dict={x: X_train[-10 * batch_size:],
                                         # x_mask: X_train_mask[-10 * batch_size:],
                                         y_: Y_train[-10 * batch_size:],
                                         dropout_keep_prob: 1.0})
        # print(train_loss)
        train_loss = '_'.join([str(i)[:6] for i in train_loss])

        dev_result = [accuracy, cross_entropy]

        if config['mask_ratio']:
            # mask_feed = np.ones_like(X_dev)
            mask_feed = np.zeros_like(X_dev)
            dev_accuracy, *dev_loss = sess.run(dev_result,
                                              feed_dict={x: X_dev,
                                                         # x_mask: X_dev_mask,
                                                         y_: Y_dev,
                                                         w_mask: mask_feed,
                                                         word_same_dropout: 1.0,
                                                         dropout_keep_prob: 1.0
                                                         })

        else:
            dev_accuracy, *dev_loss = sess.run(dev_result,
                                              feed_dict={x: X_dev,
                                                         # x_mask: X_dev_mask,
                                                         y_: Y_dev,
                                                         dropout_keep_prob: 1.0})
        if dev_accuracy > best_accuracy or dev_loss[0] < best_loss:
            timedelay = 0
            best_accuracy = dev_accuracy
            best_loss = dev_loss[0]
            # mask_feed = np.ones_like(X_test)
            mask_feed = np.zeros_like(X_test)

            save_path = saver.save(sess, log_path + '/model.ckpt')


            if config['mask_ratio']:
                test_accuracy, test_prediction = sess.run([accuracy, prediction],
                                                          feed_dict={x: X_test,
                                                                     # x_mask: X_test_mask,
                                                                     y_: Y_test,
                                                                     w_mask: mask_feed,
                                                                     word_same_dropout: 1.0,
                                                                     dropout_keep_prob: 1.0
                                                                     })
            else:
                test_accuracy, test_prediction = sess.run([accuracy, prediction],
                                         feed_dict={x: X_test,
                                                    # x_mask: X_test_mask,
                                                    y_: Y_test,
                                                    dropout_keep_prob: 1.0})


        else:
            timedelay += 1
        # break
        dev_loss = '_'.join([str(i)[:6] for i in dev_loss])

        sys.stdout.write('Epoch: %d' % (e + 1))
        sys.stdout.write('\tTrain Loss: {}'.format(train_loss))
        sys.stdout.write('\tDev Loss: {}'.format(dev_loss))
        sys.stdout.write('\tDev Accuracy: %.6f' % dev_accuracy)
        sys.stdout.write('\tTest Accuracy: %.6f' % test_accuracy)
        sys.stdout.write('\tEpoch Time: %.1fs' % (time.time() - epoch_starttime))
        sys.stdout.write('\tTimedelay: %.1fs' % timedelay)
        sys.stdout.write('\n')

    # Test trained model.
    f_wrong = open('{}/test_wrong'.format(log_path), 'w')

    f_right = open('{}/test_right'.format(log_path), 'w')
    # [1, 0] neg
    # [0, 1] pos
    id_word_map = {j: i for i, j in word_idx_map.items()}
    id_word_map[0] = 'padding'
    k = 3
    for index, pred in enumerate(test_prediction):
        sent = ' '.join([id_word_map[i] for i in X_test[index] if i > 0])
        sent_score = W_score_ref[X_test[index]]
        top_k = sent_score.argsort()[(-1) * k:][::-1]
        key_word_str = ''
        label = np.argmax(Y_test[index])

        for mm in top_k:
            word_id = X_test[index][mm]

            ratio_divide = (W_score[word_id] - 1)
            use_ratio = ratio_divide

            if word_id in w_score_come_from:
                if label == w_score_come_from[word_id]:
                    key_word_str += '{} {} {} {} {}\t'.format(id_word_map[word_id], W_score_ref[word_id], ratio_divide, use_ratio, count_ref[word_id])
        if not key_word_str:
            key_word_str = 'no good mask'

        if config['dataset'] not in ['sst1', 'trec']:
            pred = ['neg', 'pos'][pred]
            label = ['neg', 'pos'][label]
        if pred == label:
            f_right.write('{}\n{}\nref: {}\n\n'.format(sent, pred, key_word_str))
        else:
            f_wrong.write('{}\npred: {} label: {}\nref: {}\n\n'.format(sent, pred, label, key_word_str))

    f_wrong.close()
    f_right.close()

    print('CV: ' + str(data_split + 1) + ' Test Accuracy: %.4f%%\n' % (100 * test_accuracy))


    sess.close()

    return test_accuracy


# main function
def main(args, para=None):
    import config
    config = config.config
    config['baseline_same_drop'] = False
    config['dropouttrain'] = False

    if args.debug is not None:
        config['debug'] = args.debug
    if args.dataset:
        config['dataset'] = args.dataset
    if args.baseline:
        config['mask_ratio'] = False
    if args.nodropout:
        config['use_dropout'] = False
    if args.baseline_same_drop:
        config['baseline_same_drop'] = True
    if args.dropouttrain:
        config['dropouttrain'] = True
    if args.experiment:
        config['experiment'] = args.experiment
    if args.batchsize:
        config['batch_size'] = args.batchsize
    if args.beta:
        config['beta'] = args.beta
    if args.magicscale:
        config['magic_scale'] = True
    if args.random:
        config['embedding'] = 'random'
    if args.same_drop_prob:
        config['same_drop_prob'] = args.same_drop_prob



    data_path = 'data/' + config['dataset'] + '.p'

    config['config_str'] = '{}/{}/'.format(config['dataset'], 'cnn')

    if config['dropouttrain']:
        config['config_str'] += 'dropout_train'
    elif config['mask_ratio']:
        if config['baseline_same_drop']:
            config['config_str'] += 'baseline_same_drop_dropout_{}_{}_magic_{}'.format(config['use_dropout'], config['same_drop_prob'], config['magic_scale'])
        else:
            config['config_str'] += 'maskratio_beta_{}_dropout_{}_magic_{}'.format(config['beta'], config['use_dropout'], config['magic_scale'])
    else:
        config['config_str'] += 'baseline_dropout_{}_magic_{}'.format(config['use_dropout'], config['magic_scale'])

    config['config_str'] += '_{}'.format(config['embedding'])
    # print(config)
    # exit()

    padding = 4
    # padding = 0
    print('Loading Data...')
    data_file = open(data_path, 'rb')
    x = pickle.load(data_file)
    data_file.close()
    sentences, W, W2, word_idx_map, vocab, maxlen = x[0], x[1], x[2], x[3], x[4], x[5]
    if config['debug'] or config['embedding'] == 'random':
        print('use random embedding')
        W = W2 # test in computer
    print('Data Loaded!')

    experiments_all = []
    experiments = 1 if config['debug'] else config['experiment']

    for exp in range(experiments):
        final = []
        cv_num = 10 if (config['dataset'] in ['mr', 'subj', 'cr', 'mpqa']) and (not config['debug']) else 1
        for i in range(cv_num):
            log_path = 'logs/{}/experiment_{}/cv_{}'.format(config['config_str'], exp, i)
            datasets, W_score_ref, count_ref, w_score_come_from = make_idx_data_cv(sentences, word_idx_map, i, maxlen, padding, config, log_path)
            print('process done')

            if config['word_score_way'] == '1':
                W_score = np.ones(W.shape[0], dtype=np.float32)
            else:
                W_score = W_score_ref

            acc = train_cv(datasets,
                           W,
                           W_score,
                           W_score_ref,
                           count_ref,
                           w_score_come_from,
                           config,
                           log_path,
                           word_idx_map,
                           data_split=i,
                           maxlen=maxlen + 2 * padding,
                           hidden_dim=config['label_class'],
                           batch_size=config['batch_size'])
            final.append(acc)
        final = np.mean(final)
        experiments_all.append(final)
        print('Experiment {} Final Test Accuracy: {}'.format(exp, final))
    f_model = open('result', 'a')
    print('\n\nAll Experiment Final Test Accuracy: {}'.format(experiments_all))
    print('All experiments final mean : {}'.format(np.mean(experiments_all)))
    f_model.write('\n')
    f_model.write(config['config_str'])
    f_model.write('\nAll Experiment Final Test Accuracy: {}'.format(experiments_all))
    f_model.write('\nAll experiments final mean : {}\n\n'.format(np.mean(experiments_all)))


# entry point.
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    # store_true.默认为False，输入则为true
    parser.add_argument("--debug", help="default false", action='store_true')
    parser.add_argument("--dataset", help="datset to choose", type=str)
    parser.add_argument("--baseline", help="default false, cnn baseline", action='store_true')
    parser.add_argument("--baseline_same_drop", help="in dropout layer, all words dropped according to the same probability, default false", action='store_true')
    parser.add_argument("--dropouttrain", help="in fully connected layer, every w has a trained dropout probability, and are masked in this probability, default false", action='store_true')
    parser.add_argument("--nodropout", help="default false", action='store_true')
    parser.add_argument("--magicscale", help="default false", action='store_true')
    parser.add_argument("--random", help="default false", action='store_true')
    parser.add_argument("--experiment", help="experiment number", type=int)
    parser.add_argument("--beta", help="beta 0.01 0.1", type=float)
    parser.add_argument("--batchsize", help="batch size", type=int)
    parser.add_argument("--same_drop_prob", help="same_drop_prob in baseline_same_drop", type=float)


    args = parser.parse_args()
    print(args)
    main(args)
