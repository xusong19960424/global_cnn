## Purpose of this project
Neural networks are often trained via mini-batch, where global information is gathered implicitly rather than explicitly and not fully used.
We propose a method named GI-Gropout to utilize global information to help NNs training.
The effectiveness of the proposed method is demonstrated on seven datasets.

## The strengths:
1.	The method helps models to learn more inapparent features
2.	The method helps models to reduce the overfitting for the apparent features

## Requirements and preprocessing
Code is written in Python3 and requires Tensorflow (>=1.0.0).

The data preprocessing and hyper-parameter setting strictly follow the implementation in https://github.com/yoonkim/CNN_sentence [1], whose preprocessing code is reused in our project.
Pre-trained embedding 'GoogleNews-vectors-negative300.bin' is available at https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing.

## How to use:
### To process the raw data: 
`python3 process_data.py --vector_path GoogleNews-vectors-negative300.bin`

This will create a pickle object called 'dataset.p' in the same folder, which contains the dataset in the right format.
If the dataset has divided the train/test, we follow the division.
If there is on standard division, we will use cv to test the model, which is stored in dataset.p


### To train and test the model:
there are three modes, and we can choose dataset from {sst1, sst2, mr, cr, mpqa, trec, subj}
one is the baseline CNN as Kim did in https://github.com/yoonkim/CNN_sentence [1]

`CUDA_VISIBLE_DEVICES=0 python3 global_cnn.py --baseline --dataset sst2`

The second is our GI-Dropout method
You can specify the beta, like that:

`CUDA_VISIBLE_DEVICES=0 python3 global_cnn.py --dataset sst2 --beta 0.01`

We conduct another experiments to test whether global information makes key contribution, where all words are dropped according to the same probability
You can specify the same_drop_prob, like that:

`CUDA_VISIBLE_DEVICES=0 python3 global_cnn.py --baseline_same_drop --dataset sst2 same_drop_prob 0.1`

## Reference
Yoon Kim. Convolutional neural networks for sentence classification. EMNLP 2014.

The code is based on https://github.com/shenshen-hungry/Semantic-CNN