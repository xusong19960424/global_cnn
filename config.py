config = {
            'use_mask': False,
            'word_score': False,
            'timedelay': 10,
            'batch_size': 128,
            'smoothing_alpha': 1,
            'experiment': 10,
            'word_score_way': 'ratio',
            'debug':False,
            'beta': 0.05,
            'magic_scale': False,
            'embedding': 'pretrained',
            'mask_ratio':True,
            'use_dropout': True,
            'dataset': 'mr',  # mr, cr, sst2, sst5, subj, trec, mpga
            'same_drop_prob': 0.1
        }



